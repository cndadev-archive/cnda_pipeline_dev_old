#!/bin/bash

source @PIPELINE_DIR_PATH@/scripts/AFNI_setup.sh
export BXH_XCEDE_TOOLS_HOME=/nrgpackages/tools/bxh_xcede_tools
PATH=${BXH_XCEDE_TOOLS_HOME}/bin:${PATH}
export PATH
