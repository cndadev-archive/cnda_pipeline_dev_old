#!/bin/bash

FREESURFER_HOME=/nrgpackages/tools/freesurfer5

if [ -z "$FSLDIR" ]; then
  source @PIPELINE_DIR_PATH@/scripts/fsl5_setup.sh
fi
source ${FREESURFER_HOME}/SetUpFreeSurfer.sh
QA_SCRIPTS=$FREESURFER_HOME/QAtools
RECON_CHECKER_SCRIPTS=$QA_SCRIPTS/data_checker
PATH=$RECON_CHECKER_SCRIPTS:$QA_SCRIPTS:$PATH
export PATH FREESURFER_HOME
