#!/bin/bash

# The NIl tools are on the path by default.
# We want to remove that and set it up ourselves.
PATH=`sed 's#/nrgpackages/tools/nil-tools##' <<< $PATH`

# Make sure to set up NIL tools after FS, so NIL tools will take precidence on the PATH
source /data/CNDA/pipeline/scripts/freesurfer53-patch_setup.sh
source /data/CNDA/pipeline/scripts/nil-tools_setup.sh


PUP_DIR=/nrgpackages/tools.release/CNDA/PUP-20170511

atlpath=/nrgpackages/atlas

PATH=$PUP_DIR/bin:$PUP_DIR/scripts:$PATH

export PATH PUP_DIR atlpath
